let listoption = document.querySelectorAll('.comboxdrop .seleteoption');
let combobtn = document.querySelector('.mycombobox')

listoption.forEach(item => {
    item.addEventListener('click', () => {
        document.querySelector('#seleteditem').innerHTML = item.innerHTML;

        setTimeout(() => {
            combobtn.style.borderBottom = "1px solid #727171";
            combobtn.style.borderRadius = "0.2rem";
        }, 350);
    });
})

$('.dropdowncombobox').on('hide.bs.dropdown', function () {
    $(this).find('.dropdown-menu').first().stop(true, true).slideUp();

    setTimeout(() => {
        combobtn.style.borderBottom = "1px solid #727171";
        combobtn.style.borderRadius = "0.2rem";
    }, 350);

    $('.mycombobox').mouseover(function () {
        combobtn.style.borderBottom = "1px solid #727171";
    });

    $('.mycombobox').mouseout(function () {
        combobtn.style.borderBottom = "1px solid #4a5258";
    });
});

$('.dropdowncombobox').on('show.bs.dropdown', function() {
    $(this).find('.dropdown-menu').first().stop(true, true).slideDown();

    combobtn.style.borderBottom = "0px";
    combobtn.style.borderRadius = "0.2rem 0.2rem 0 0";

    $('.mycombobox').mouseover(function () {
        combobtn.style.borderBottom = "0px";
    });

    $('.mycombobox').mouseout(function () {
        combobtn.style.borderBottom = "0px";
    });
});
